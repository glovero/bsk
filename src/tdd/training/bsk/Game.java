package tdd.training.bsk;

public class Game {
	
	Frame frames[];
	int firstBonus, secondBonus;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		frames = new Frame[10];
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		boolean done = false;
		for (int i = 0; i < 10; i++) {
			if (frames[i] == null) {
				frames[i] = frame;
				done = true;
				break;
			}
		}
		
		if(!done) {
			throw new BowlingException("Non � possibile aggiungere un altro frame!");
		}
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		if ((index >= 0) && (index <= 9)) {
			return frames[index];
		} else throw new BowlingException("Non esiste un " + index + "� frame!");	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		firstBonus = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		secondBonus = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonus;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonus;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score[] = new int[10];
		int totalScore = 0;
		score[0] = frames[0].getScore();
		for (int i = 1; i < 10; i++) {
			if (frames[i - 1].isStrike()) {
				score[i - 1] = frames[i - 1].getScore() + frames[i].getScore();
			} else if (frames[i - 1].isSpare()) {
				score[i - 1] = frames[i - 1].getScore() + frames[i].getFirstThrow();
			} else score[i - 1] = frames[i - 1].getScore();
		}
		
		for (int i = 2; i < 10; i++) {
			if ((frames[i - 1].isStrike()) && (frames[i - 2].isStrike())) {
				score[i] = frames[i].getScore();
				score[i - 1] = frames[i - 1].getScore() + frames[i].getScore();
				score[i - 2] = frames[i - 2].getScore() + frames[i - 1].getScore() + frames[i].getFirstThrow();
			}
		}
		
		if(frames[9].isStrike()) {
			score[9] = frames[9].getScore() + getFirstBonusThrow() + getSecondBonusThrow();
		} else if(frames[9].isSpare()) {
			score[9] = frames[9].getScore() + getFirstBonusThrow();
		} else score[9] = frames[9].getScore();
		
		if((frames[9].isStrike()) && (getFirstBonusThrow() == 10)) {
			score[8] = frames[9].getScore() + frames[9].getScore() + getFirstBonusThrow();
		}
		
		for (int i = 0; i < 10; i++) {
			totalScore += score[i];
		}
		return totalScore;	
	}

}
