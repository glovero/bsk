package tdd.training.bsk;

public class Frame {
	
	private int fThrow, sThrow;
	private int bonusFrame = 0;

	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		if(firstThrow + secondThrow > 10) {
			throw new BowlingException("Numero di birilli incorretto!");
		}
		else {
			fThrow = firstThrow;
			sThrow = secondThrow;
		}
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		return fThrow;
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		return sThrow;
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
		bonusFrame = bonus;
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		return bonusFrame;
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {
		if (isStrike()) {
			return fThrow + sThrow + getBonus();
		} else if(isSpare()) {
			return fThrow + sThrow + getBonus();
		} else return fThrow + sThrow;
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
		if((fThrow == 10) && (sThrow == 0)) {
			return true;
		} else return false;
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		if(((fThrow + sThrow) == 10) && (sThrow != 0)) {
			return true;
		} else return false;
	}

}
