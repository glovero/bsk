package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;


public class FrameTest {

	@Test
	public void testFirstThrow() throws BowlingException{
		int firstThrow = 2;
		int secondThrow = 4;
		Frame frame = new Frame(firstThrow, secondThrow);
		assertEquals(firstThrow, frame.getFirstThrow());
	}
	
	@Test
	public void testSecondThrow() throws BowlingException{
		int firstThrow = 2;
		int secondThrow = 4;
		Frame frame = new Frame(firstThrow, secondThrow);
		assertEquals(secondThrow, frame.getSecondThrow());
	}
	
	@Test
	public void testFrameScore() throws BowlingException{
		int firstThrow = 2;
		int secondThrow = 4;
		Frame frame = new Frame(firstThrow, secondThrow);
		assertEquals((firstThrow + secondThrow), frame.getScore());
	}
	
	@Test
	public void testIsSpare() throws BowlingException{
		boolean isSpare = true;
		Frame frame = new Frame(5, 5);
		assertEquals(isSpare, frame.isSpare());
	}
	
	@Test
	public void testGetBonus() throws BowlingException{
		Frame frame = new Frame(3, 6);
		frame.setBonus(3);
		assertEquals(3, frame.getBonus());
	}
	
	@Test
	public void testGetScoreOfASpareFrame() throws BowlingException{
		Frame frame1 = new Frame(1, 9);
		Frame frame2 = new Frame(3, 6);
		frame1.setBonus(frame2.getFirstThrow());
		assertEquals(13, frame1.getScore());
	}
	
	@Test
	public void testIsStrike() throws BowlingException{
		boolean isStrike = true;
		Frame frame = new Frame(10, 0);
		assertEquals(isStrike, frame.isStrike());
	}
	
	@Test
	public void testGetScoreOfAStrikeFrame() throws BowlingException{
		Frame frame1 = new Frame(10, 0);
		Frame frame2 = new Frame(3, 6);
		frame1.setBonus(frame2.getScore());
		assertEquals(19, frame1.getScore());
	}
}
